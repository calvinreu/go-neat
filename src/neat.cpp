#include "neat.hpp"

void Init(const uint &inputCount, const uint &outputCount, const uint &populationSize, const float &compabilittyThreshhold, const float &weightImportance, const float &excessGeneImportance, const float &disjointGeneImportance, const float &deathRate)
{
    GlobalInputCount = inputCount;
    GlobalOutputCount = outputCount;
    GlobalNodeCount = inputCount + outputCount;
    GlobalNonHiddenNodeCount = inputCount + outputCount;
    PopulationSize = populationSize;
    CompabilittyThreshhold = compabilittyThreshhold;
    WeightImportance = weightImportance;
    ExcessGeneImportance = excessGeneImportance;
    DisjointGeneImportance = disjointGeneImportance;
    DeathRate = deathRate;
    Innovations = 0;
    
    PublicGenom.Init();

    for (auto i = 0; i < populationSize; i++)
    {
        genom currentGenom;
        bool foundSpecies = false;
        for (auto j = Species.begin(); j != Species.end(); j++)
        {
            if (j->addGenom(currentGenom))
            {
                foundSpecies = true;
                break;
            }
        }

        if (!foundSpecies)
            Species.push_back(species(currentGenom));
    }

    for (auto i = Species.begin(); i != Species.end(); i++)
        i->createNNs();
    

    std::cout << Species.size() << std::endl;
    std::cout << "NEAT:INIT\n";
}

void NewGeneration()
{
    std::list<genom> children;
    AverageScore = 0;

    for (auto i = Species.begin(); i != Species.end(); i++)
        i->calculateScores();
    
    AverageScore /= NNPopulation.size();

    for (auto i = Species.begin(); i != Species.end(); i){
        i->trim();
        auto iNext = i;
        iNext++;
        if (i->getGRef() == nullptr) 
            Species.erase(i);
        
        i = iNext;
    }

    for (auto i = NNPopulation.begin(); i != NNPopulation.end(); i++)
        i->score = 0;

    while (NNPopulation.size() + children.size() < PopulationSize)
    {
        for (auto i = Species.begin(); i != Species.end(); i++)
            i->getChildren(children);
    }

    for (auto i = children.begin(); i != children.end(); i++) {
        i->mutate();
        i->addChild();
    }

    auto iter = children.begin();
    auto iterNext = iter;
    while (children.size() != 0)
    {
        bool foundSpecies = false;
        iterNext++;
        for (auto i = Species.begin(); i != Species.end(); i++)
        {
            if (i->addGenom(iter, children))
            {
                foundSpecies = true;
                break;
            }

        }
        if (!foundSpecies)
            Species.emplace_front(iter, children);

        iter = iterNext;
    }

}

void RunNNs(const uint &times, evaluation_func evaluate)
{
    for (size_t i = 0; i != times; i++)
        for (auto nn = NNPopulation.begin(); nn != NNPopulation.end(); nn++)
            nn->score += evaluate(*nn);
}

void Train(const uint &Generations, const uint &runsPerGen, evaluation_func evaluate) {
    for (size_t i = 0; i < Generations; i++)
    {
        RunNNs(runsPerGen, evaluate);
        NewGeneration();
    }   
}

void PrintScore(const uint &runCount, const uint &nnCount, evaluation_func evaluate) {
    uint nnNumber = 0;
    for (size_t i = 0; i < runCount; i++) {
        for (auto nn = NNPopulation.begin(); nn != NNPopulation.end(); nn++)
            nn->score += evaluate(*nn);
    }

    std::multimap<float, NeuralNetwork*> Leaderboard;

    for (auto i = NNPopulation.begin(); i != NNPopulation.end(); i++) {
        Leaderboard.insert(std::make_pair(i->score, &(*i))); 
        i->score = 0;
    }

    for (auto i = Leaderboard.begin(); i != Leaderboard.end() && nnNumber != nnCount; nnNumber++, i++)
    {
        for(size_t j = 0; j != runCount; j++)
            i->second->score += evaluate(*i->second);

        std::cout << i->second->score << std::endl;
        i->second->score = 0;
    }
}