#include "public_values.h"

pGenom PublicGenom;
std::list<NeuralNetwork> NNPopulation;
std::list<species> Species;
uint GlobalNodeCount, GlobalInputCount, GlobalOutputCount, GlobalNonHiddenNodeCount, PopulationSize, Innovations;
float WeightImportance, ExcessGeneImportance, DisjointGeneImportance, CompabilittyThreshhold, DeathRate, AverageScore;


float RandomWeightP = 0.1;
float MutateNodeP = 0.1;
float MutateLinkP = 0.1;
float MaxRandomWeight =2;
float MaxWeightShift = 0.1;