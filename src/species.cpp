#include "species.hpp"

extern float CompabilittyThreshhold;



const genom* species::getGRef() const {    return gRef;}

species::species(const genom &initialGenom) {
    genoms.push_back(initialGenom);
    gRef = &genoms.front();
}

species::species(genom &&initialGenom) {
    genoms.push_back(initialGenom);
    gRef = &genoms.front();
}

species::species(const std::list<genom>::iterator &srcElement, std::list<genom> &srcList) {
    genoms.splice(genoms.begin(), srcList, srcElement);
    gRef = &genoms.front();
}

bool species::addGenom(const genom &genom_) {
    if (CompabilittyThreshhold < gRef->difference(genom_))
        return false;

    genoms.push_back(genom_);
    return true;
}

bool species::addGenom(genom &&genom_) {
    if (CompabilittyThreshhold < gRef->difference(genom_))
        return false;

    genoms.push_back(genom_);
    return true;
}

bool species::addGenom(const std::list<genom>::iterator &srcElement, std::list<genom> &srcList) {
    if (CompabilittyThreshhold < gRef->difference(*srcElement))
        return false;

    genoms.splice(genoms.begin(), srcList, srcElement);
    return true;    
}

void species::createNNs() {
    for (auto i = genoms.begin(); i != genoms.end(); i++)
        i->addChild();
}

void species::calculateScores() {
    for (auto i = genoms.begin(); i != genoms.end(); i++)
        i->calculateScore(); 
}

void species::trim() {
    if(genoms.size() == 1 && genoms.front().getScore() < AverageScore){
        gRef = nullptr;
        return;
    }

    std::multimap<float, std::list<genom>::iterator> leaderboard;
    for (auto i = genoms.begin(); i != genoms.end(); i++)
        leaderboard.insert(std::make_pair(i->getScore(), i));

    uint trimSize = genoms.size()*DeathRate;
    auto iNext=leaderboard.begin();
    for (auto i = leaderboard.begin(); trimSize > 0; trimSize--)
    {
        iNext++;
        genoms.erase(i->second);
        i=iNext;
    }
    gRef = &genoms.front();
}

void species::getChildren(std::list<genom> &children) {
    for (auto i = genoms.begin(); i != genoms.end(); i++)
        children.emplace_front(*i);
}