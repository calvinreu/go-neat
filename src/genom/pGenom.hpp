#pragma once
#include <list>
#include <set>
#include <map>
#include <vector>
#include "../types.hpp"
#include "gene.hpp"
#include "../util/difference.hpp"
#include <iostream>

extern float WeightImportance, DisjointGeneImportance, ExcessGeneImportance;
extern uint GlobalNodeCount, GlobalInputCount, GlobalOutputCount;

class pGenom
{
private:
    std::list<pGene> genes;
public:
    pGenom();
    std::map<uint, gene> CreateCompleteGenom();
    void Print() const; 
    void addNode(pGene *gene_);
    pGene* getGene(const pGene &gene_);
    void Init();
    std::list<pGene>::iterator NullChild();
};