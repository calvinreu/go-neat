#pragma once
#include <list>
#include <set>
#include <map>
#include <vector>
#include "../types.hpp"
#include "gene.hpp"
#include "../util/difference.hpp"

extern float WeightImportance, DisjointGeneImportance, ExcessGeneImportance;
extern uint GlobalNodeCount, GlobalInputCount, GlobalOutputCount;
extern float RandomWeightP, MutateNodeP, MutateLinkP, MaxRandomWeight, MaxWeightShift;

class genom
{
private:
    std::map<uint, gene> genes;
    std::list<NeuralNetwork>::iterator child;
    float score = 0;
    void mutateNode();
    void mutateLink();
    void mutateWeight();
    inline uint mutateLinkOut(const std::set<uint> &nodes) const;
    inline uint mutateLinkIn(const std::set<uint> &nodes) const;
    inline void After(std::set<uint> &nodes, const uint &in) const;
    void TraceRoot(std::multimap<uint, uint> &structure, std::set<uint> &nodes, const std::multimap<uint, uint>::iterator &in) const;
public:
    void Print() const;
    //void setChild(const std::list<NeuralNetwork>::iterator &child);
    std::list<NeuralNetwork>::iterator getChild();
    // crossover this as a better scoring genom with the other genom
    genom crossover(const genom &other);
    const std::map<uint, gene>& getGenes() const;
    std::set<uint> getNodes() const;
    void addGene(gene &&gene_);
    void mutate();
    void addChild();
    const float& getScore() const;
    void calculateScore();
    float difference(const genom &other) const;
    // return the number of species this genome is compatible with
    float overallCompabilitty() const;
    genom(genom &&other);
    genom(const genom &other);
    genom();
    ~genom();
};
