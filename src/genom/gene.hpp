#pragma once
#include "../types.hpp"
#include "genom.hpp"
#include "pGenom.hpp"
#include "../util/random.hpp"

extern pGenom PublicGenom;
typedef unsigned int uint;
extern uint Innovations;

//public gene
class pGene
{
private:
    uint in, out, innovationNumber;//node input output
    std::list<pGene>::iterator child;//if there has been a node created on the connection points to the first gene which is new
public:
    pGene(const uint &in, const uint &out);
    void addNode();
    bool operator==(const pGene &other) const;
    ~pGene();

    const uint& GetIn()  const;
    const uint& GetOut() const;
    const uint& GetInnovation() const;
    //get child connection to the created node
    const pGene& GetChildPrev() const;
    //get child connection from the created node
    const pGene& GetChildNext() const;

    friend gene;
    friend pGenom;
};

class gene
{
private:
    pGene* innovationNumber;
    float weight;
    bool enabled;
public:
    //sort by innovation number
    bool operator<(const gene &other) const;
    const uint& GetInnovationNumber() const;
    const float& GetWeight() const;
    const bool &IsEnabled() const;
    void Disable();
    const uint& GetIn () const;
    const uint& GetOut() const;
    void ShiftWeight();
    void RandomizeWeight();
    const uint &GetNext() const;
    const uint &GetPrev() const;

    void mutateNode(genom *parent);

    gene(pGene *gene_, const float &weight);

    friend pGenom;
};
